# uonet-api-docs [![Discord](https://discordapp.com/api/guilds/367325058353594378/embed.png)]()

## Nieoficjalna dokumentacja API dla UONET+ tworzonego przez firmę Vulcan

Dokumentacja tworzona jest na podstawie analizy ruchu sieciowego tworzonego przez aplikację [Dzienniczek+](https://play.google.com/store/apps/details?id=pl.vulcan.uonetmobile) na telefonie z systemem Android - [metoda Man In The Middle](https://pl.wikipedia.org/wiki/Atak_man_in_the_middle) i kodu otwartoźródłowego projektu [uonet-sdk](https://github.com/VLO-GDA/uonet-sdk).